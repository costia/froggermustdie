var game = new Phaser.Game(1280, 720, Phaser.AUTO, 'phaser', 
	{ preload: preload, create: create,update: update ,render: render });
var scoreTextsStyle = { font: "48px Arial", fill: "#ff0044", align: "center",boundsAlignH: "center", boundsAlignV: "middle"};
var buttonTextStyle = { font: "32px Arial", fill: "#000000",align: "center",boundsAlignH: "center", boundsAlignV: "middle"};
var multiplierTextStyle = { font: "32px Arial", fill: "#000000",align: "center",boundsAlignH: "center", boundsAlignV: "middle"};
var buttonLocations= [22.5,37.5,62.5,77.5]
var scoreText

var resolution=[1280,720]
var maxFrogs=20
var score = 0
var buttonKeyMap=['q','1','w','2','e','3','r','4']
var Frogs=[]
var cars=[]
var buttons=[]
var tick=0
var spriteGroup

Array.prototype.clean = function(deleteValue) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue) {         
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};

function Frog(sprite=undefined){
	this.offsetTick=Math.random()*10000;
	this.speed=Math.random()*6+1
	this.x=(0.15+Math.random()*0.7)*resolution[0]
	if (Math.random()>0.5){
		this.y=resolution[1]*9.5/10
		this.speed=-this.speed
	}else{
		this.y=resolution[1]*0.5/10	}
	this.cycle=Math.random()*100+50;
	this.dutycycle=(Math.random()*0.1+0.2)*this.cycle	
	this.startTick=Math.floor(tick+600*Math.random())

	if (sprite==undefined){
		this.sprite=game.add.sprite(this.x, this.y, 'Frog');
	}else{//reuse sprite
		this.sprite=sprite
		this.sprite.x=this.x
		this.sprite.y=this.y
	}
	this.sprite.anchor.setTo(0.5, 0.5);
	this.sprite.visible = false;
	if (this.speed>0){
		this.sprite.angle=180
	}else{
		this.sprite.angle=0
	}
	frogGroup.add(this.sprite)
}

function Button(id){
		this.id=id
		this.active=true
		if (id%2==0){
			this.direction=-1
			this.x=resolution[0]*19/20.0
			this.y=resolution[1]*buttonLocations[Math.floor((id)/2)]/100.0
		}else{
			this.direction=1
			this.x= resolution[0]/20
			this.y= resolution[1]*buttonLocations[Math.floor((id)/2)]/100.0 
		}
		this.sprite = game.add.sprite(this.x,this.y, 'button');
		this.sprite.anchor.setTo(0.5, 0.5);
		this.text=game.add.text(0,0, buttonKeyMap[id].toUpperCase(), buttonTextStyle);

		this.text.setTextBounds(this.x-this.sprite.width/2,this.y-this.sprite.height/2,
				this.sprite.width,this.sprite.height);
		this.sprite.inputEnabled = true;
		this.sprite.events.onInputDown.add(this.onPressed, this);
		uiGroup.add(this.sprite)
		uiGroup.add(this.text)

}

Button.prototype.onPressed=function(){
	if (this.active) {
		this.active=false
		this.sprite.alpha=0
		this.sprite.tint = 0x808080
		this.tween=game.add.tween(this.sprite).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None);
		this.tween.onComplete.add(
			function(){
				this.active=true
				this.sprite.tint = 0xffffff
			},this
		)
		this.tween.start()
		cars.push(new Car(this.x+resolution[0]*this.direction*0.1,
				this.y,this.direction))
	}
}


function Car(x,y,dir){
	this.x=x
	this.y=y+dir*resolution[1]*0.035
	this.speed=dir*6
	this.scoreMult=1
	this.sprite=game.add.sprite(this.x,this.y, 'car');
	if (dir>0){
		this.sprite.angle=180
	}
	this.sprite.anchor.setTo(0.5, 0.5);
	carGroup.add(this.sprite)
}

Car.prototype.destroy=function (){
	this.sprite.destroy()
}


function Splat(x,y){
	this.sprite=game.add.sprite(x,y, 'splat');
	this.sprite.angle=Math.random()*360
	var tween = game.add.tween(this.sprite).to( { alpha: 0 }, 1000, "Linear", true, 2000);
	tween.onComplete.add(function(){this.sprite.destroy()},this)
	fx_splat.play()
	splatGroup.add(this.sprite)
}

function MultiplierPopup(x,y,mult){
	this.text=game.add.text(x,y,'x'+mult, multiplierTextStyle);
	this.text.anchor.setTo(0.5, 0.5);
	var tween = game.add.tween(this.text).to( { alpha: 0 }, 2000, "Linear", true, 1000);
	tween.onComplete.add(function(){this.text.destroy()},this)
	uiGroup.add(this.text)
}

function preload(){
    game.load.image('background', 'images/background.png');
    game.load.image('button', 'images/button.png');
    game.load.image('car', 'images/car.png');
    game.load.image('Frog', 'images/frog.png');
    game.load.image('splat', 'images/splat.png');

    game.load.audio('ambientFrog', ['audio/73019_hanstimm_frogszm.ogg']);
    game.load.audio('engine', ['audio/319038_kwahmah-02_engine-running-loop.ogg']);
    game.load.audio('splat', ['audio/Spit_Splat-Mike_Koenig-1170500447.ogg']);
}

function create(){
	uiGroup=game.add.group()
	frogGroup=game.add.group()
	carGroup=game.add.group()
	splatGroup=game.add.group()
	bgGroup=game.add.group()

	spriteGroup=game.add.group()
	spriteGroup.add(bgGroup)
	spriteGroup.add(splatGroup)
	spriteGroup.add(frogGroup)
	spriteGroup.add(carGroup)
	spriteGroup.add(uiGroup)

	fx_ambientFrog = game.add.audio('ambientFrog');
	fx_ambientFrog.loop=true
	fx_ambientFrog.play()
	
	fx_ambientEngine = game.add.audio('engine');
	fx_ambientEngine.volume=0.1
	fx_ambientEngine.play()

	fx_splat = game.add.audio('splat');
	fx_splat.allowMultiple = true;

	fx_engine = game.add.audio('engine');

    var backgroundSprite=game.add.sprite(0, 0, 'background');	
    bgGroup.add(backgroundSprite)

    var scoreString='SCORE: '+score
    scoreText = game.add.text(resolution[0]/2, 0, scoreString, scoreTextsStyle);
    scoreText.anchor.setTo(0.5,0)
	uiGroup.add(scoreText)

    for (i=0;i<maxFrogs;i++){
		Frogs[i]=new Frog()
    }

    var precents=[22.5,37.5,62.5,77.5]
	for (i=0;i<8;i++){
		buttons[i]=new Button(i);
	}

	game.input.keyboard.onDownCallback = keyPressEvent
}

function keyPressEvent(e){
	for (i=0;i<8;i++){
		if (e.key==buttonKeyMap[i]){
			buttons[i].onPressed()
			break;
		}
	}
}
function update() {
	frogMovementUpdate()
	carMovementUpdate()
	collisionsCheck()
	tick++;
}

function frogMovementUpdate(){
	for (i=0;i<maxFrogs;i++){
		if (Frogs[i]==undefined) continue;
		frog=Frogs[i];
		if (frog.startTick==tick){
			frog.sprite.visible = true;
		}else if (frog.startTick<tick){
			var currentTick=(tick+frog.offsetTick)%frog.cycle
			if (currentTick<frog.dutycycle){
				frog.y=frog.y+frog.speed
				if ((frog.y<(resolution[1]*0.02)) || (frog.y>(resolution[1]*0.98))){ 
					Frogs[i]=new Frog(frog.sprite)
				}
			}
		}
	}
}

function carMovementUpdate(){
	for (i=0;i<cars.length;i++) {
		if (cars[i]==undefined) continue;
		car=cars[i];
		car.x=car.x+car.speed
		if ((car.x<(resolution[0]*0.12))|| (car.x>(resolution[0]*0.88) )){
			car.destroy();
			cars[i]=undefined
		}
	}
	cars.clean(undefined)
	if ((cars.length>0)&&(!fx_engine.isPlaying)){
		fx_engine.play();
	}else if ((cars.length==0)&&(fx_engine.isPlaying)){
		fx_engine.stop()
	}
}


function collisionsCheck(){
	for (i=0;i<cars.length;i++) {
		if (cars[i]==undefined) continue;
		for (j=0;j<Frogs.length;j++){
			if (Frogs[j]==undefined) continue;
			dx=Math.abs(cars[i].x-Frogs[j].x)
			dy=Math.abs(cars[i].y-Frogs[j].y)
			if ((dx<(32+16))&&(dy<(19+16))){
				score+=10*cars[i].scoreMult
				var splat= new Splat(Frogs[j].x,Frogs[j].y)
				if (cars[i].scoreMult>1){
					var multPopup=new MultiplierPopup(Frogs[j].x,Frogs[j].y,cars[i].scoreMult)
				}
				Frogs[j]=new Frog(Frogs[j].sprite)
				cars[i].scoreMult++
			}
		}
	}

}

function render(){
	for (i=0;i<Frogs.length;i++){
		if (Frogs[i]==undefined) continue;
		Frogs[i].sprite.x=Frogs[i].x;
		Frogs[i].sprite.y=Frogs[i].y;
	}
	
	for (i=0;i<cars.length;i++){
		if (cars[i]==undefined) continue;
		cars[i].sprite.x=cars[i].x;
		cars[i].sprite.y=cars[i].y;
	}

	scoreText.text='SCORE: '+score

}